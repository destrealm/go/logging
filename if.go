// Defined interfaces and API constructs.
//
// The definitions here are intentionally somewhat simple. The BasicLogger
// interface encompasses any logger that implements the core of Go's `log`
// package. The ExtendedLogger interface covers our implementation.

package logging

import (
	"io"
)

type BasicLogger interface {
	Fatal(...interface{})
	Fatalf(string, ...interface{})
	Fatalln(...interface{})
	Panic(...interface{})
	Panicf(string, ...interface{})
	Panicln(...interface{})
	Print(...interface{})
	Printf(string, ...interface{})
	Println(...interface{})
	Writer() io.Writer
}

type ExtendedLogger interface {
	Error(...interface{})
	Errorf(string, ...interface{})
	Warn(...interface{})
	Warning(...interface{})
	Warnf(string, ...interface{})
	Warningf(string, ...interface{})
	Notice(...interface{})
	Noticef(string, ...interface{})
	Info(...interface{})
	Information(...interface{})
	Infof(string, ...interface{})
	Informationf(string, ...interface{})
	Debug(...interface{})
	Debugf(string, ...interface{})

	BasicLogger
}
