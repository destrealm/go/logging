// Logger: A logging facility helper for Golang.
//
// Golang's standard library ships with a powerful and useful logging facility.
// However, the standard logger doesn't always mesh well with all use cases,
// particularly when it may be necessary to segregate messages into
// classifications based upon the severity of the error condition. There are
// other open source logging facilities available, but most are either similarly
// deficient to Golang's standard logger or require unnecessarily complicated
// configuration in order to work.
//
// Logger attempts to remedy these shortcomings by providing a logging facility
// with sensible (and usable!) defaults right out of the box. Extra features can
// be configured as needed but are not required. Most importantly, Logger uses
// the Go standard library for features such as format templating, and logging
// backends are as easy to configure as passing in something that implements
// the io.Writer interface.
//
// More advanced use cases are also supported with minimal effort. In
// particular, it's possible to configure a logger's backend such that only
// specific error levels are logged. Or, for more elaborate requirements,
// multiple loggers can be chained together such that everything is logged to a
// file in one logger, but only error conditions are logged to the console in
// another. Logger is intended to be as flexible as you require or as minimal as
// you prefer.
//
// Copyright (c) 2015-2020, Benjamin Shelton. All rights reserved.
//
// This file is distributed under the NCSA license. For the complete license
// text, please refer to the LICENSE file accompanying this distribution or view
// it at https://git.destrealm.org/go/logging/LICENSE

package logging
