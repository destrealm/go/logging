// Logging initialization functions.
//
// See doc.go for a more thorough package description.
//
// This package borrows a few rather genius ideas from go-chi, namely TTY
// detection: https://github.com/go-chi/chi
//
// Copyright (c) 2015-2019, Benjamin Shelton. All rights reserved.
//
// This file is distributed under the NCSA license. For the complete license
// text, please refer to the LICENSE file accompanying this distribution or view
// it at https://git.destrealm.org/go/logging/LICENSE

package logging

import (
	"os"
)

var isTTY bool

func init() {
	stat, err := os.Stdout.Stat()
	if err == nil {
		m := os.ModeDevice | os.ModeCharDevice
		isTTY = stat.Mode()&m == m
	}
}
