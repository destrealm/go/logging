// Logger base implementation.
//
// See doc.go for a more thorough package description.
//
// Copyright (c) 2015-2020, Benjamin Shelton. All rights reserved.
//
// This file is distributed under the NCSA license. For the complete license
// text, please refer to the LICENSE file accompanying this distribution or view
// it at https://git.destrealm.org/go/logging/LICENSE

package logging

import (
	"fmt"
	"io"
	"os"
	"sync"
	"text/template"
	"time"
)

// Internal loggers. This map contains all loggers that were registered with the
// logging package. This is safe for concurrent access.
//
// We use a sync.Map for two reasons: 1) Concurrent read access from multiple
// goroutines is a must and 2) write access is comparatively infrequently and
// unlikely to result in key collisions. Namely, this matches the intent of
// sync.Map quite well (write once, read many).
var loggers sync.Map

// Formatter instance. This is used internally for the formatter template.
type Formatter struct {
	Timestamp  time.Time
	Time       string
	DateFormat string
	Error      string
	ErrorLevel Level
	Message    string
	ShortError string
}

// FormatTime is the time formatting function. This is useful if client code
// wishes to use a Go template pipeline to change the format of the timestamp.
//
// This is a convenience method and isn't required.
func (f *Formatter) FormatTime(format string) string {
	return f.Timestamp.Format(format)
}

// SyncWriter provides an implementation of io.Writer that synchronizes logging
// messages such that interleaving output over a shared writer won't happen.
//
// When using Logging's default settings, the internal reference to os.Stdout
// will be wrapped.
type SyncWriter struct {
	io.Writer
	sync.Mutex
}

// Write implementation satisfying io.Writer guarded by an internal mutex.
func (w *SyncWriter) Write(p []byte) (int, error) {
	w.Lock()
	n, err := w.Writer.Write(p)
	w.Unlock()
	return n, err
}

// WrapWriter accepts an io.Writer and returns it, guarded by a SyncWriter. This
// is the recommended entry point for implementations to utilize SyncWriters.
func WrapWriter(w io.Writer) *SyncWriter {
	return &SyncWriter{
		w,
		sync.Mutex{},
	}
}

// FormatterFunc defines the type of a formatter function that can be used to
// override the use of Go templates. This function must accept the logger
// instance as its first argument followed by a single formatter (containing the
// message data) and return a string.
type FormatterFunc func(*Log, *Formatter) string

// Log container.
type Log struct {
	// Template format string.
	format string

	// Formatter override. Use this if you don't want to use the internal template
	// parser or have specific needs that the template mechanism cannot provide
	// (post processing?). Sample functions are provided, including some that
	// colorize the output.
	//
	// Overrides can be provided per level. Use SetDefaultFormatOverride to set a
	// default format override function. Otherwise, use AddFormatOverride for
	// adding an override function for a specific level.
	formatOverride map[Level]FormatterFunc

	// FuncMap (needs docs).
	funcs template.FuncMap

	// Compiled template.
	tpl *template.Template

	// Level for this logger.
	level Level

	// Logger timezone. UTC is recommended.
	tz *time.Location

	// Format for date/time output.
	dateFormat string

	// This mutex covers the backends, log chain, and options metadata.
	sync.Mutex

	// Current (or default) output backend.
	backend io.Writer

	// Additional backends, if required (typically this will be empty).
	backends []io.Writer

	// Logger chain.
	chain *Log

	// Metadata options. Typically this map will be empty and isn't used
	// internally by the logger. However, it is provided for formatterOverrides
	// that may need additional metadata for configuration (such as enabling or
	// disabling colorized output, among other things).
	Options map[string]interface{}
}

// MustInheritLogger always returns a logger instance that is either derived
// from the named parent logger or is itself a new logger instance. All parent
// properties are replicated onto the child.
func MustInheritLogger(name, parent string) *Log {
	logger, ok := loggers.Load(name)
	if ok {
		if l, ok := logger.(*Log); ok {
			return l
		}
	}

	p, ok := loggers.Load(parent)
	if !ok {
		return NewLogger(name)
	}
	pl, ok := p.(*Log)
	if !ok {
		return NewLogger(name)
	}

	lg := NewLogger(name)
	lg.format = pl.format
	lg.formatOverride = pl.formatOverride
	lg.funcs = pl.funcs
	lg.tpl = pl.tpl
	lg.level = pl.level
	lg.tz = pl.tz
	lg.dateFormat = pl.dateFormat
	lg.backend = pl.backend
	lg.backends = pl.backends
	lg.chain = pl.chain
	lg.Options = pl.Options

	return lg
}

// MustGetLogger always returns a logger instance associated with the name
// `name`, creating it if it doesn't exist.
func MustGetLogger(name string) *Log {
	logger, ok := loggers.Load(name)
	if !ok {
		return NewLogger(name)
	}

	if l, ok := logger.(*Log); ok {
		return l
	}

	return NewLogger(name)
}

// MustGetDefaultLogger returns the default logger and is a convenience function
// for `MustGetLogger("__main__")`. This name is chosen to avoid conflicts with
// other loggers used by client applications.
func MustGetDefaultLogger() *Log {
	return MustGetLogger("__name__")
}

// MustGetLoggerWithLevel always returnr a logger instance associated with the
// name `name` at the specified logging level, creating it if it doesn't exist.
// If the logger has already been created, no attempt will be made to reset the
// level. Useful if you wish to otherwise avoid clobbering a
// previously-configured logger instance.
func MustGetLoggerWithLevel(name string, level Level) *Log {
	logger, ok := loggers.Load(name)
	if !ok {
		lg := NewLogger(name)
		lg.SetLevel(level)
		return lg
	}

	if l, ok := logger.(*Log); ok {
		return l
	}

	lg := NewLogger(name)
	lg.SetLevel(level)
	return lg
}

// MustGetDefaultLoggerWithLevel returns the default logger with the specified
// logging level. This is a convenience function for calling
// `MustGetLoggerWithLevel("__main__", level)` and is primarily intended to
// provide an easy to remember call for configuring a primary (or default)
// logger for client application. "__main__" is chosen to avoid conflicts with
// other loggers.
func MustGetDefaultLoggerWithLevel(level Level) *Log {
	return MustGetLoggerWithLevel("__main__", level)
}

// NewLogger creates and returns a new logger instance with the name `name`,
// associating it with the logger container in this module. This will always
// create a new logger.
//
// If you simply wish to retrieve a logger instance if it exists (creating it if
// it does not), use MustGetLogger() instead.
func NewLogger(name string) *Log {
	logger := &Log{
		format:         "[{{.ShortError}}] {{.Time}} - {{.Message}}",
		funcs:          make(map[string]interface{}),
		formatOverride: make(map[Level]FormatterFunc),
		level:          Error,
		tz:             time.UTC,
		dateFormat:     "2006-01-02 15:04:05 MST",
		backend:        WrapWriter(os.Stdout),
		backends:       []io.Writer{nil, nil, nil, nil, nil, nil, nil, nil},
		Options:        make(map[string]interface{}),
	}
	loggers.Store(name, logger)
	logger.tpl = template.Must(template.New("logger").Funcs(template.FuncMap(logger.funcs)).Parse(logger.format))
	return logger
}

// Configurational methods.

// AddFormatFunc inserts a formatting function to the template handler.
func (l *Log) AddFormatFunc(name string, f interface{}) {
	l.funcs[name] = f

	if len(l.funcs) > 0 {
		l.tpl.Funcs(template.FuncMap(l.funcs))
	}
}

// AddFormatOverride injects an override function for the level specified.
func (l *Log) AddFormatOverride(level Level, f FormatterFunc) {
	l.Lock()
	defer l.Unlock()
	l.formatOverride[level] = f
}

// SetDefaultFormatOverride sets the default format override function for the
// current logger.
//
// Note that internally, this stores the "default" as a level of -1, which is
// otherwise impossible to do via exposed methods.
func (l *Log) SetDefaultFormatOverride(f FormatterFunc) {
	l.Lock()
	defer l.Unlock()
	l.formatOverride[-1] = f
}

// ChainLogger chains another Log instance with the current logger. When a
// logger is configured to chain to another logger, any logging messages will be
// repeatedly dispatched down the chain until no more logger instances are
// found. In this way, it's possible to isolate multiple logger behaviors by
// error level.
//
// For example, if you wish to create two loggers, one that logs only error
// messages (and nothing else) and another that logs everything (including error
// messages), such as might be necessary for applications that must write error
// messages to a file as well as to STDOUT, you can do the following:
//
//  errorsOnly := &bytes.Buffer{}
//
//  // Error logger.
//  errorLog := MustGetLogger("errors") errorLog.SetBackend(nil)
//
//  // Clear the main backend.
//  errorLog.SetBackendForLevel(Error, errorLog)
//
//  // Everything else (remember, the default is to log to os.Stdout)...
//  everythingLog := MustGetLogger("everything")
//
//  // Chain the loggers starting with the most restrictive.
//  errorLog.ChainLogger(everything)
//
//  // Now, when we call errorLog.*, it will always log to everythingLog.
//  errorLog.Error("This is an error. It is written to both.")
//  errorLog.Debug("This is a debug notice. It is written only to
//      everythingLog.")
func (l *Log) ChainLogger(logger *Log) {
	l.chain = logger
}

// SetBackend changes the default backend. This backend will be written to
// whenever any of the logging facility methods (Error, Critical, etc.) are
// called.
//
// If you wish to override this backend for a specific logging level, use
// SetBackendForLevel.
//
// Setting a backend with this function will wrap the given writer in a
// SyncWriter if it isn't already.
func (l *Log) SetBackend(backend io.Writer) *Log {
	_, ok := backend.(*SyncWriter)
	if !ok {
		backend = WrapWriter(backend)
	}
	l.backend = backend
	return l
}

// SetRawBackend behaves identically to SetBackend with the exception that it
// does not wrap the backend argument in a SyncWriter.
func (l *Log) SetRawBackend(backend io.Writer) *Log {
	l.backend = backend
	return l
}

// SetBackendForLevel changes the backend to use for a specific level.
// Configuring this value will override the default backend for the specified
// level only.
//
// For instance, creating the following:
//
//  buf := &bytes.Buffer{}
//  logger := MustGetLogger("example")
//  logger.SetBackendForLevel(Error, buf)
//
// Will cause:
//
//  logger.Error("some error")
//
// to save logging data to `buf`, while
//
//  logger.Critical("critical error!")
//  logger.Debug("some debugging information")
//
// will both write to the default backend (os.Stdout).
//
// To clear the backend for the specified level, reverting it back to using the
// default backend, call ClearBackend(level).
//
// Setting a backend with this function will wrap the given writer in a
// SyncWriter if it isn't already.
func (l *Log) SetBackendForLevel(level Level, backend io.Writer) *Log {
	_, ok := backend.(*SyncWriter)
	if !ok {
		backend = WrapWriter(backend)
	}
	l.backends[level] = backend
	return l
}

// SetRawBackendForLevel behaves identically to SetBackendForLevel with the
// exception that it does not wrap the backend argument in a SyncWriter.
func (l *Log) SetRawBackendForLevel(level Level, backend io.Writer) *Log {
	l.backends[level] = backend
	return l
}

// ClearBackend removes the backend for the specified level.
func (l *Log) ClearBackend(level Level) *Log {
	l.backends[level] = nil
	return l
}

// SetFormat sets the logger format. Logger formats are derived from a defined
// Go text template into which the log message and timestamp are fed. At
// present, the following fields are available:
//
//  .Time       - Timestamp
//  .Error      - Error level text (one of Emergency, Alert, Critical, Error,
//                Warning, Notice, Info, or Debug.
//  .ErrorLevel - Error level (as a number between 0-7).
//  .Message    - Log message.
//  .ShortError - Shortened error level text (one of EMRG, ALRT, CRTC, ERRR,
//                WARN, NOTC, INFO, DBUG)
//
// By default, this is "{{.Time}} - {{.Message}}\n" (note that you must include
// a newline where appropriate--it will not be added for you automatically.
//
// If you're using colorizer libraries (like github.com/agtorre/gocolorize), you
// can manipulate that here. Future versions might allow for template pipes to
// be configured, but for now you can use Go's template control structures to
// change the color according to the error level.
func (l *Log) SetFormat(format string) *Log {
	l.format = format
	l.tpl = template.Must(template.New("logger").Funcs(template.FuncMap(l.funcs)).Parse(format))
	return l
}

// SetLevel changes the logger's level. Error messages that are at this
// level--or lower--will be logged; everything else will be ignored. Note that
// in this context "lower" means the literal integer value of the logger and not
// the relative importance of its data. For example, setting a logging level of
// Error will log anything at an Error or below (which includes Alert,
// Emergency, and Critical). Likewise, setting a logging level of Debug will log
// literally everything.
//
// If you wish to control what the logger instance logs such as logging only
// Error-level messages, you'll need to manipulate the backends. This can be
// done by setting a backend for the Error level and clearing the default
// backend by setting it to nil.
func (l *Log) SetLevel(level Level) *Log {
	l.level = level
	return l
}

// SetOption is the recommended method for setting metadata values in the
// logger. It's possible to set options by manipulating the Log.Options map
// directly but there is no stability guarantee provided for its continued
// export status.
func (l *Log) SetOption(option string, value interface{}) {
	l.Options[option] = value
}

// SetTimezone sets the logger's timezone to the specified location. The logger
// uses time.UTC by default. Change this to time.Local to display the server's
// local time.
func (l *Log) SetTimezone(tz *time.Location) *Log {
	if tz == nil {
		tz = time.UTC
	}
	l.tz = tz
	return l
}

// Error logs an error condition.
func (l *Log) Error(v ...interface{}) {
	l.log(Error, v...)
}

// Errorf logs an error condition with formatting.
func (l *Log) Errorf(format string, v ...interface{}) {
	l.logf(Error, format, v...)
}

// Warn logs a warning.
func (l *Log) Warn(v ...interface{}) {
	l.log(Warning, v...)
}

// Warning is an alias for Warn.
func (l *Log) Warning(v ...interface{}) {
	l.log(Warning, v...)
}

// Warnf logs a warning with formatting.
func (l *Log) Warnf(format string, v ...interface{}) {
	l.logf(Warning, format, v...)
}

// Warningf is an alias for Warnf.
func (l *Log) Warningf(format string, v ...interface{}) {
	l.logf(Warning, format, v...)
}

// Notice logs a notice containing pertinent information. This does not indicate
// an error condition.
func (l *Log) Notice(v ...interface{}) {
	l.log(Notice, v...)
}

// Noticef logs a notice containing pertinent information with formatting. This
// does not indicate an error condition.
func (l *Log) Noticef(format string, v ...interface{}) {
	l.logf(Notice, format, v...)
}

// Info logs an informational message.
func (l *Log) Info(v ...interface{}) {
	l.log(Info, v...)
}

// Information is an alias for Info.
func (l *Log) Information(v ...interface{}) {
	l.log(Info, v...)
}

// Infof logs an informational message with formatting.
func (l *Log) Infof(format string, v ...interface{}) {
	l.logf(Info, format, v...)
}

// Informationf is an alias for Infof.
func (l *Log) Informationf(format string, v ...interface{}) {
	l.logf(Info, format, v...)
}

// Debug logs debugging messages.
func (l *Log) Debug(v ...interface{}) {
	l.log(Debug, v...)
}

// Debugf logs debugging messages with formatted input.
func (l *Log) Debugf(format string, v ...interface{}) {
	l.logf(Debug, format, v...)
}

// Go standard library call implementations for compatibility.

// Printf works similarly to the Go standard library log.Printf with the
// exception that it's treated as an informational message.
func (l *Log) Printf(format string, v ...interface{}) {
	l.logf(Info, format, v...)
}

// Print works similarly to the Go standard library log.Print. Like Printf, it's
// treated as an informational message.
func (l *Log) Print(v ...interface{}) {
	l.log(Info, v...)
}

// Println works similarly to the Go standard library log.Println. Like Printf
// and Print, it's treated as an informational message.
func (l *Log) Println(v ...interface{}) {
	l.log(Info, v...)
}

// Fatalf works identically to the Go standard library log.Fatalf, returning an
// error status and exiting the application. It additionally logs the message
// as Fatal.
func (l *Log) Fatalf(format string, v ...interface{}) {
	l.logf(Fatal, format, v...)
	os.Exit(1)
}

// Fatal works similarly to the Go standard library log.Fatal. Like Fatalf, it's
// treated as as an error condition and exits the application. It additionally
// logs the message as Fatal.
func (l *Log) Fatal(v ...interface{}) {
	l.log(Fatal, v...)
}

// Fatalln works similarly to the Go standard library log.Fatalln. Like Fatalf
// and Fatal, it's treated as an error condition and exits the application. It
// additionally logs the message as Fatal.
func (l *Log) Fatalln(v ...interface{}) {
	l.log(Fatal, v...)
}

// Panicf works identically to the Go standard library log.Panicf, printing the
// error and returning a panic. As with Fatalf, this logs the message as fatal.
func (l *Log) Panicf(format string, v ...interface{}) {
	l.logf(Fatal, format, v...)
	panic(fmt.Sprintf(format, v...))
}

// Panic works similarly to the Go standard library log.Panic. Like Panicf, it's
// treated as as an error condition and panics. It additionally logs the message
// as fatal.
func (l *Log) Panic(v ...interface{}) {
	l.log(Fatal, v...)
}

// Panicln works similarly to the Go standard library log.Panicln. Like Panicf
// and Panic, it's treated as a fatal error and returns a panic.
func (l *Log) Panicln(v ...interface{}) {
	l.log(Fatal, v...)
}

// Writer is provided to satisfy the BasicLogger interface as provided by the go
// `log` package. This will return either the configured backend, all backends,
// or both, depending on what is available. Backend levels are ignored.
func (l *Log) Writer() io.Writer {
	backends := make([]io.Writer, 1, len(l.backends)+1)
	if len(l.backends) == 0 {
		return l.backend
	}

	if l.backend != nil {
		backends[0] = l.backend
	}
	backends = append(backends, l.backends...)
	return io.MultiWriter(backends...)
}

// Logs a message at the specified verbosity level.
func (l *Log) log(level Level, v ...interface{}) {
	var err error
	message := fmt.Sprintln(v...)

	if l.chain != nil {
		l.chain.log(level, v...)
	}

	if level > l.level {
		return
	}

	tpl := &Formatter{
		Timestamp:  time.Now().In(l.tz),
		DateFormat: l.dateFormat,
		Error:      Levels[level],
		ErrorLevel: level,
		Message:    message,
		ShortError: level.Short(),
	}

	tpl.Time = tpl.Timestamp.Format(l.dateFormat)

	fn := l.getFormatOverrideForLevel(level)

	l.Lock()
	if l.backends[level] == nil {
		if l.backend != nil {
			if fn == nil {
				err = l.tpl.Execute(l.backend, tpl)
			} else {
				_, err = fmt.Fprint(l.backend, fn(l, tpl))
			}
		}
	} else {
		if fn == nil {
			err = l.tpl.Execute(l.backends[level], tpl)
		} else {
			_, err = fmt.Fprint(l.backends[level], fn(l, tpl))
		}
	}
	l.Unlock()

	if err != nil {
		fmt.Fprintf(os.Stderr, "failed to log output: %s\n", err)
	}
}

// Retrieves the format override function for the specified level, default
// function (if defined), or nil.
func (l *Log) getFormatOverrideForLevel(level Level) FormatterFunc {
	if f, ok := l.formatOverride[level]; ok {
		return f
	}

	// -1 is the "default" override.
	if f, ok := l.formatOverride[-1]; ok {
		return f
	}

	return nil
}

// Logs a message at the specified verbosity level with the provided formatting
// string (compatiable with Printf).
func (l *Log) logf(level Level, format string, v ...interface{}) {
	l.log(level, fmt.Sprintf(format, v...))
}
