// This file provides rough compatibility with logging requirements for one of
// Go's most popular cron libraries, github.com/robfig/cron.

package logging

type cron struct {
	*Log
}

// Cron wraps the specified logger with an implementation that provides an
// interface compatible with cron.Logger.
func Cron(log *Log) *cron {
	return &cron{
		log,
	}
}

// Error differs from destrealm/go/logging in the nature of its arguments and
// the intended format of the message. We currently (and deliberately) avoid
// some of the string building exercises used by cron to create key/value output
// as this is not supported by logging at this time.
func (c *cron) Error(err error, msg string, kv ...interface{}) {
	c.Log.Errorf("%s: error: %s; %v", msg, err, kv)
}

// Info differs from destrealm/go/logging in the nature of its arguments and the
// intended format of the message. See Error().
func (c *cron) Info(msg string, kv ...interface{}) {
	c.Log.Infof("%s: %v", msg, kv)
}
