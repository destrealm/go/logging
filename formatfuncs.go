// Stock format override functions.
//
// See doc.go for a more thorough package description.
//
// This package borrows a few rather genius ideas from go-chi, namely color
// definitions and TTY detection.
//
// Copyright (c) 2015-2020, Benjamin Shelton. All rights reserved.
//
// This file is distributed under the NCSA license. For the complete license
// text, please refer to the LICENSE file accompanying this distribution or view
// it at https://git.destrealm.org/go/logging/LICENSE

package logging

var (

	// Terminal colors. Borrowed from go-chi:
	// github.com/go-chi/chi

	// Black escape sequence (normal font type).
	Black = []byte{0x33, '[', '3', '0', 'm'}

	// Red escape sequence (normal font type).
	Red = []byte{0x33, '[', '3', '1', 'm'}

	// Green escape sequence (normal font type).
	Green = []byte{0x33, '[', '3', '2', 'm'}

	// Yellow escape sequence (normal font type).
	Yellow = []byte{0x33, '[', '3', '3', 'm'}

	// Blue escape sequence (normal font type).
	Blue = []byte{0x33, '[', '3', '4', 'm'}

	// Magenta escape sequence (normal font type).
	Magenta = []byte{0x33, '[', '3', '5', 'm'}

	// Cyan escape sequence (normal font type).
	Cyan = []byte{0x33, '[', '3', '6', 'm'}

	// White escape sequence (normal font type).
	White = []byte{0x33, '[', '3', '7', 'm'}

	// Reset escape sequence.
	Reset = []byte{0x33, '[', '0', 'm'}
)
