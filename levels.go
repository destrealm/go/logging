// Levels and associated functions.
//
// See doc.go for a more thorough package description.
//
// Copyright (c) 2015-2020, Benjamin Shelton. All rights reserved.
//
// This file is distributed under the NCSA license. For the complete license
// text, please refer to the LICENSE file accompanying this distribution or view
// it at https://git.destrealm.org/go/logging/LICENSE

package logging

import (
	"strings"
)

// Levels defines the supported RFC 5424 logging levels in this package.
var Levels = []string{
	"FATAL",
	"ERROR",
	"WARNING",
	"NOTICE",
	"INFO",
	"DEBUG",
}

// Level defines a type for use internally for identifying logging levels.
type Level int

// String implementation.
func (l Level) String() string {
	switch l {
	case Fatal:
		return "FATAL"
	case Error:
		return "ERROR"
	case Warning:
		return "WARN"
	case Notice:
		return "NOTICE"
	case Info:
		return "INFO"
	case Debug:
		return "DEBUG"
	default:
		return ""
	}
}

// Short string implementation.
func (l Level) Short() string {
	switch l {
	case Fatal:
		return "FATL"
	case Error:
		return "ERRO"
	case Warning:
		return "WARN"
	case Notice:
		return "NOTE"
	case Info:
		return "INFO"
	case Debug:
		return "DBUG"
	default:
		return ""
	}
}

// RFC 5424 levels (borrowed from syslog message facilities).
const (
	Fatal Level = iota
	Error
	Warning
	Notice
	Info
	Debug
)

// LevelFromString returns a Level value (internally an integer) given a
// matching string. This serves as a simple map between string -> integer values
// of error levels.
//
// Some levels have aliases. INFO and WARN can be given as "information" or
// "warning," respectively.
//
// The case of input values is ignored.
func LevelFromString(l string) Level {
	switch strings.ToLower(l) {
	case "fatal":
		return Fatal
	case "error":
		return Error
	case "warning", "warn":
		return Warning
	case "notice":
		return Notice
	case "info", "information":
		return Info
	}
	return Debug
}
