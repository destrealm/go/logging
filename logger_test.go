// Copyright (c) 2015-2020, Benjamin Shelton. All rights reserved.
//
// This file is distributed under the NCSA license. For the complete license
// text, please refer to the LICENSE file accompanying this distribution or view
// it at https://git.destrealm.org/go/logging/LICENSE

package logging_test

import (
	"bytes"
	"fmt"
	"io/ioutil"
	"strings"
	"testing"
	"time"

	"git.destrealm.org/go/logging"
)

func TestCreateNewLogger(t *testing.T) {
	buf := &bytes.Buffer{}

	logger := logging.NewLogger("test")
	logger.SetBackend(buf)
	logger.Error("This is a test.")

	if !strings.Contains(buf.String(), "This is a test.") {
		t.Errorf(`log entry does not contain "This is a test.", got "%s" instead`, buf.String())
	}

	if !strings.Contains(buf.String(), "\n") {
		t.Error(`log entry must be terminated with a newline`)
	}
}

func TestMustGetLogger(t *testing.T) {
	buf := &bytes.Buffer{}

	logger := logging.MustGetLogger("TestMustGetLogger")
	logger.SetBackend(buf)
	logger.Error("This is a test.")

	if !strings.Contains(buf.String(), "This is a test.") {
		t.Errorf(`log entry does not contain "This is a test.", got "%s" instead`, buf.String())
	}
}

func TestMustGetLoggerWithLevel(t *testing.T) {
	buf := &bytes.Buffer{}

	// Create a logger to verify we're not clobbering it.
	logger := logging.MustGetLogger("TestMustGetLoggerWithLevel")
	logger.SetBackend(buf)
	logger.SetLevel(logging.Info)

	logger = logging.MustGetLoggerWithLevel("TestMustGetLoggerWithLevel", logging.Error)

	logger.Info("This is a test.")

	if !strings.Contains(buf.String(), "This is a test.") {
		t.Errorf(`MustGetLoggerWithLevel appears to have clobbered the pre-existing logger; received "%s" instead of "This is a test."`,
			buf.String())
	}
}

func TestSetLevel(t *testing.T) {
	buf := &bytes.Buffer{}

	logger := logging.MustGetLoggerWithLevel("TestSetLevel", logging.Info)
	logger.SetBackend(buf)
	logger.SetLevel(logging.Error)

	logger.Info("This should not be logged.")

	if strings.Contains(buf.String(), "This should not be logged.") {
		t.Errorf(`Setting level with SetLevel should inhibit more verbose logging when set to a lower verbosity (starting at Info and reducing to Error, as in this case)`)
	}
}

func BenchmarkStdlibDiscard(b *testing.B) {
	for n := 0; n < b.N; n++ {
		fmt.Fprintf(ioutil.Discard, "Logging output.")
	}
}

func BenchmarkLogger(b *testing.B) {
	logger := logging.MustGetLogger("BenchmarkLogger")
	logger.SetBackend(ioutil.Discard)
	for n := 0; n < b.N; n++ {
		logger.Info("Logging output.")
	}
}

func TestLoggerChain(t *testing.T) {
	buf1 := &bytes.Buffer{}
	buf2 := &bytes.Buffer{}

	logger1 := logging.NewLogger("test1")
	logger1.SetBackend(buf1)
	logger2 := logging.NewLogger("test2")
	logger2.SetBackend(buf2)

	logger1.ChainLogger(logger2)

	logger1.Error("This is a test.")

	if !strings.Contains(buf1.String(), "This is a test.") {
		t.Errorf(`logger1 should generate "This is a test.", got "%s" instead`, buf1.String())
	}

	if !strings.Contains(buf2.String(), "This is a test.") {
		t.Errorf(`logger2 should generate "This is a test.", got "%s" instead`, buf2.String())
	}
}

func BenchmarkLoggerChain(b *testing.B) {
	logger1 := logging.MustGetLogger("BenchmarkLoggerChain")
	logger2 := logging.NewLogger("BenchmarkLoggerChain2")

	logger1.SetBackend(ioutil.Discard)
	logger2.SetBackend(ioutil.Discard)
	logger1.ChainLogger(logger2)

	for n := 0; n < b.N; n++ {
		logger1.Info("Logging output.")
	}
}

func TestBackends(t *testing.T) {
	errBuf := &bytes.Buffer{}
	everythingElse := &bytes.Buffer{}

	logger := logging.NewLogger("test")
	logger.SetLevel(logging.Notice)
	logger.SetBackend(everythingElse)
	logger.SetBackendForLevel(logging.Error, errBuf)

	logger.Error("This is an error.")
	logger.Notice("This is everything else.")

	if !strings.Contains(errBuf.String(), "This is an error.") {
		t.Errorf(`logger.Error should generate "This is an error.", got "%s" instead`, errBuf.String())
	}

	if !strings.Contains(everythingElse.String(), "This is everything else.") {
		t.Errorf(`logger.Notice should generate "This is everything else.", got "%s" instead`, everythingElse.String())
	}
}

func BenchmarkBackendNotice(b *testing.B) {
	logger := logging.MustGetLoggerWithLevel("BenchmarkBackendNotice",
		logging.Notice)
	logger.SetBackend(ioutil.Discard)
	logger.SetBackendForLevel(logging.Error, ioutil.Discard)

	for n := 0; n < b.N; n++ {
		logger.Notice("Logging output.")
	}
}

func BenchmarkBackendError(b *testing.B) {
	logger := logging.MustGetLoggerWithLevel("BenchmarkBackendError",
		logging.Notice)
	logger.SetBackend(ioutil.Discard)
	logger.SetBackendForLevel(logging.Error, ioutil.Discard)

	for n := 0; n < b.N; n++ {
		logger.Notice("Logging output.")
	}
}

func TestSetTimezone(t *testing.T) {
	buf := &bytes.Buffer{}
	loc, _ := time.LoadLocation("America/Denver")
	ct := time.Now()
	ct.In(loc)

	logger := logging.NewLogger("test")
	logger.SetBackend(buf)
	logger.SetTimezone(loc)

	logger.Error("This is a test.")

	if !strings.Contains(buf.String(), ct.Format("MST")+" - This is a test.") {
		t.Errorf(`logger.Error should have generated "%s - This is a test." in the output, got "%s" instead`,
			ct.Format("MST"), buf.String())
	}
}

func TestSetFormatBasic(t *testing.T) {
	buf := &bytes.Buffer{}

	logger := logging.NewLogger("test")
	logger.SetBackend(buf)
	logger.SetFormat(`[prefix] - {{.Time}} - {{.Message}}`)

	logger.Error("This is a test.")

	if buf.String()[0:8] != "[prefix]" {
		t.Errorf(`logger output should start with a prefix: "[prefix]", got "%s" instead`, buf.String())
	}
}

func TestSetFormatFunc(t *testing.T) {
	buf := &bytes.Buffer{}

	logger := logging.NewLogger("test")
	logger.SetBackend(buf)
	logger.SetLevel(logging.Info)
	logger.AddFormatFunc("autoprefix", func(level logging.Level) string {
		if level == logging.Error {
			return "!! Error !!"
		} else if level == logging.Warning {
			return "** WARN **"
		}
		return ""
	})
	logger.SetFormat(`[{{autoprefix .ErrorLevel}}] - {{.Time}} - {{.Message}}`)

	logger.Error("This is a test.")

	if buf.String()[0:13] != "[!! Error !!]" {
		t.Errorf(`logger output should start with prefix "[!! Error !!]" generated by the autoprefix function, got "%s" instead`, buf.String())
	}
}

func TestLevels(t *testing.T) {
	logger := logging.NewLogger("test")
	buffers := make([]*bytes.Buffer, len(logging.Levels))
	for i, _ := range logging.Levels {
		buffers[i] = &bytes.Buffer{}
		logger.SetBackendForLevel(logging.Level(i), buffers[i])
	}

	logger.SetLevel(logging.Debug)

	logger.Error("This is a regular ol' error.")
	logger.Warn("This is a warning.")
	logger.Notice("This is a notice.")
	logger.Info("This is an informational message.")
	logger.Debug("This is some debugging output.")

	if !strings.Contains(buffers[logging.Error].String(),
		"This is a regular ol' error.") {
		t.Errorf(`logger.Error() should contain "This is a regular ol' error."; got "%s" instead`, buffers[3].String())
	}

	if !strings.Contains(buffers[logging.Warning].String(),
		"This is a warning.") {
		t.Errorf(`logger.Warn() should contain "This is a warning."; got "%s" instead`, buffers[4].String())
	}

	if !strings.Contains(buffers[logging.Notice].String(),
		"This is a notice.") {
		t.Errorf(`logger.Notice() should contain "This is a notice."; got "%s" instead`, buffers[5].String())
	}

	if !strings.Contains(buffers[logging.Info].String(),
		"This is an informational message.") {
		t.Errorf(`logger.Info() should contain "This is an informational message."; got "%s" instead`, buffers[logging.Info].String())
	}

	if !strings.Contains(buffers[logging.Debug].String(),
		"This is some debugging output.") {
		t.Errorf(`logger.Debug() should contain "This is some debugging output."; got "%s" instead`, buffers[logging.Debug].String())
	}
}

func TestLevelAliases(t *testing.T) {
	logger := logging.NewLogger("test")
	buffers := make(map[string]*bytes.Buffer)

	buffers["warning"] = &bytes.Buffer{}
	buffers["information"] = &bytes.Buffer{}

	logger.SetBackendForLevel(logging.Warning, buffers["warning"])
	logger.SetBackendForLevel(logging.Info, buffers["information"])
	logger.SetLevel(logging.Debug)

	logger.Warning("This is a warning.")
	logger.Information("This is an informational message.")

	if !strings.Contains(buffers["warning"].String(), "This is a warning.") {
		t.Errorf(`logger.Warning() should contain "This is a warning."; got "%s" instead`, buffers["warning"].String())
	}

	if !strings.Contains(buffers["information"].String(), "This is an informational message.") {
		t.Errorf(`logger.Information() should contain "This is an informational message."; got "%s" instead`, buffers["information"].String())
	}
}

func TestLevelAliasesWithFormatting(t *testing.T) {
	logger := logging.NewLogger("test")
	buffers := make(map[string]*bytes.Buffer)

	buffers["warning"] = &bytes.Buffer{}
	buffers["information"] = &bytes.Buffer{}

	logger.SetBackendForLevel(logging.Warning, buffers["warning"])
	logger.SetBackendForLevel(logging.Info, buffers["information"])
	logger.SetLevel(logging.Debug)

	logger.Warningf("This is a warning.")
	logger.Informationf("This is an informational message.")

	if !strings.Contains(buffers["warning"].String(), "This is a warning.") {
		t.Errorf(`logger.Warningf() should contain "This is a warning."; got "%s" instead`, buffers["warning"].String())
	}

	if !strings.Contains(buffers["information"].String(), "This is an informational message.") {
		t.Errorf(`logger.Informationf() should contain "This is an informational message."; got "%s" instead`, buffers["information"].String())
	}
}

func TestFormattedOutput(t *testing.T) {
	logger := logging.NewLogger("test")
	buffers := make([]*bytes.Buffer, len(logging.Levels))
	for i := range logging.Levels {
		buffers[i] = &bytes.Buffer{}
		logger.SetBackendForLevel(logging.Level(i), buffers[i])
	}

	logger.SetLevel(logging.Debug)

	logger.Errorf("This is a regular ol' %s.", "error")
	logger.Warnf("This is a %s.", "warning")
	logger.Noticef("This is a %s.", "notice")
	logger.Infof("This is an %s message.", "informational")
	logger.Debugf("This is some %s output.", "debugging")

	if !strings.Contains(buffers[logging.Error].String(),
		"This is a regular ol' error.") {
		t.Errorf(`logger.Errorf() should allow formatting`)
	}

	if !strings.Contains(buffers[logging.Warning].String(),
		"This is a warning.") {
		t.Errorf(`logger.Warnf()/logger.Warning() should allow formatting`)
	}

	if !strings.Contains(buffers[logging.Notice].String(),
		"This is a notice.") {
		t.Errorf(`logger.Noticef() should allow formatting`)
	}

	if !strings.Contains(buffers[logging.Info].String(),
		"This is an informational message.") {
		t.Errorf(`logger.Infof()/logger.Informationf() should allow formatting`)
	}

	if !strings.Contains(buffers[logging.Debug].String(),
		"This is some debugging output.") {
		t.Errorf(`logger.Debugf() should allow formatting`)
	}
}

func TestFormatOverrideFunctionOutput(t *testing.T) {
	buffer := &bytes.Buffer{}
	logger := logging.NewLogger("override-test")
	logger.SetBackend(buffer)

	fn := func(level *logging.Log, data *logging.Formatter) string {
		return fmt.Sprint("Override: " + data.Time + " - " + data.Message)
	}

	logger.SetDefaultFormatOverride(fn)
	logger.Error("This is a test of the default format override function.")

	if !strings.Contains(buffer.String(), "Override") {
		t.Error("format override did not output expected string, got:",
			buffer.String())
	}
}

func BenchmarkFormatOverrideFunction(b *testing.B) {
	logger := logging.MustGetLogger("BenchmarkLogger")
	logger.SetBackend(ioutil.Discard)

	fn := func(level *logging.Log, data *logging.Formatter) string {
		return fmt.Sprint(data.Time + " [" + data.ErrorLevel.String() + "] - " + data.Message)
	}

	logger.SetDefaultFormatOverride(fn)
	for n := 0; n < b.N; n++ {
		logger.Info("Logging output.")
	}
}

func BenchmarkFormatOverrideFunctionSprintf(b *testing.B) {
	logger := logging.MustGetLogger("BenchmarkLogger")
	logger.SetBackend(ioutil.Discard)

	fn := func(level *logging.Log, data *logging.Formatter) string {
		return fmt.Sprintf("%s [%s] - %s", data.Time, data.ErrorLevel,
			data.Message)
	}

	logger.SetDefaultFormatOverride(fn)
	for n := 0; n < b.N; n++ {
		logger.Info("Logging output.")
	}
}
